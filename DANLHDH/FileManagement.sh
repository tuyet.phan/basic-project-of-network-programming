#! /bin/bash

# set color
esc=$'\e[0m'                           
red="$bold$(tput setaf 1)"
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5) #pinky
cyan=$(tput setaf 6)
white=$(tput setaf 7)


#down_line
function down_line() {
    echo -e "\n"
}
#list files
function list_file() {  
    echo -e -n "\n${yellow}List files of CURRENT directory: ${esc}"
    pwd
    down_line
    ls -la
}

# Choose the path to do changes
function change_path() {
    echo -e -n "\n${yellow}Current path: ${esc}"
    pwd
    echo -e -n "\n${cyan}Give the path of folder that you want to do changes! or Enter . to choose current path:${esc}"
    i=0
    until [ $i -eq 1 ]
    do
    read a
    if [ ! -d $a ];
    then
        echo -e -n "\n${red}$a is no such a directory. Re-enter the path:${esc}"
        
    else
        cd $a
        echo -e "\n${red}Change path successfully!${esc}"
        echo -e -n "\n${green}Current directory is ${esc}"
        pwd
        i=1
    fi
    done
    down_line 
}

#create file
function create_file() {
    list_file
    echo -n "${cyan}Enter file_name you want to create file: ${esc}"
    read f
    if [ -f "$f" ]
    then
        echo "${red}$f already exists. You can modify it or create other file.${esc}"
    else
        touch $f
        echo "${magenta}$f is created${esc}"
        down_line
        echo "${red}Create a file Successfully!${esc}"
        list_file
    fi
    down_line
}

#write to file
function write_file() {
    list_file
    echo -n -e "\n${cyan}Enter file_name you want to write file: ${esc}"
    read f
    if [ ! -f "$f" ]
    then
        echo "${red}$f doesn't exist.${esc}"
    else
        vi $f
        echo -e "\n${red}Write a file Successfully!${esc}\n"
        echo -e "\n${yellow}The content of $f after writed:${esc}"
        cat $f
    fi
    down_line
}

#read a file
function read_file() {
    list_file
    echo -n "${cyan}Enter name file you want to READ: ${esc}"
    read f
    if [ ! -f "$f" ]
    then
        echo "${red}$f is not exist${esc}"
    elif [ -s "$f" ] 
    then
        echo -e "\n${cyan}The content of ${f}: ${esc}"
        cat $f
    else
        echo "${red}This file is a empty file! ${esc}"
    fi
    down_line
}

function delete_file() {
    echo -e "\n${red}List files BEFORE deleting a file${esc} \n"
    ls -la
    echo -n "${cyan}Enter filename you want to DELETE: ${esc}"
    read f
    if [ ! -f "$f" ]
    then
        echo "${red}$f is not exist${esc}"
    else
        rm -r $f
        echo -e "\n${red}Deleting a file successfully!${esc}"
        echo -e "${yellow}$f is deleted.${esc}"
        echo -e "\n${red}List file AFTER deleting $f!${esc} \n"
        ls -la
        
    fi
    down_line
}

function grant_permission() {
    echo -e "\n${yellow}List files${esc} ${red}BEFORE modify!${esc} \n"
    ls -la 
    echo -n "${cyan}Enter the name of file you want to make changes:${esc}"
    read f
    if [ ! -f "$f" ]
    then
    echo "${red}$f is not exist${esc}"
    else
    echo -e "\n${red}--------------------------MENU-----------------------------${esc}"
    echo "${yellow}|Enter 1 for READ,WRITE,EXECUTE to user, group and others  |${esc}"
    echo "${yellow}|Enter 2 for READ and WRITE to users and group             |${esc}"
    echo "${yellow}|Enter 3 for READ WRITE and EXECUTE to only users          |${esc}"
    echo "${red}-----------------------------------------------------------${esc}"
    echo -n "${cyan} Enter your choice: ${esc}"
    read m
    case "$m" in
        1) chmod 777 $f;;
        2) chmod 660 $f;;
        3) chmod 700 $f;;
    esac
    echo -e "\n${yellow}List files${esc} ${red}APTER modify!${esc} \n"
    ls -la -l
    fi
    down_line
}

function show_information_file() {
    list_file
    echo -n "${cyan}Enter name of file you want to see the information and store it in status file: ${esc}"
    read g
    echo ":"
    echo -e "\n ${magenta}Information of file is${esc} \n"
    stat $g
    touch status.txt
    stat $g>>status.txt
    echo "${red}Information of file is stored in file name status and its path is $a status.${esc}"
    down_line
}


function move_files() {
    i=0
    until [ $i -eq 1 ]
    do
    echo -n "${cyan}Enter the path of SOURCE directory where you want to copy the files: ${esc}"
    read p
    echo -n "${cyan}Enter the path of DESTINATION directory where you want to move the files: ${esc}"
    read q
    if [ -d "$p" ]
    then
        if [ -d "$q" ]
        then
            i=1
        else
            echo -e "\n${red} Please enter the right DESTINATION directory!${esc}\n"
        fi
    else
        if [ -d "$q" ]
        then
            echo "${red} Please enter the right SOURCE directory!${esc} "
        else
            echo "${red} Please enter the right SOUCE & DESTINATION directory!${esc} "
        fi                
    fi
    done
    if [ $i -eq 1 ]
    then
    cd $p
    echo -e "\n${yellow}LIST FILES of SOURCE directory: ${esc}"
    pwd
    ls -la
    echo -n "${magenta}Enter a number of files you want to move: ${esc}"
    read count
    for(( i=1 ; i<=$count ; i++ ))
    do
        echo -n "${cyan}Enter the name of file you want to move: ${esc}"
        read name
        mv $p/$name  $q        
    done
        cd $q
        echo -e "\n${yellow}LIST FILES of DESTINATION directory $q after moving successful:${esc} \n "
        ls -la 
    fi
    down_line
}

function exit_program() {
    echo -e "\n${yellow}Thank you! \nSee you soon! ${esc}"
    exit 0
}

#Main 
echo -e "\n${yellow}                          WELCOME TO FILE SYSTEM MANAGEMENT PROGRAM${esc}\n"
while :
do
    echo "${yellow}------------------------------------------MENU----------------------------------------------${esc}"
    echo "|   ${yellow}0${esc}. CHANGE paths to modify file in system.                                              |"
    echo "|   ${yellow}1${esc}. LIST all files in current directory.                                                |"
    echo "|   ${yellow}2${esc}. CREATE a file                                                                       |"
    echo "|   ${yellow}3${esc}. WRITE a file                                                                        |"
    echo "|   ${yellow}4${esc}. READ  a file                                                                        |"
    echo "|   ${yellow}5${esc}. DELETE a files                                                                      |"
    echo "|   ${yellow}6${esc}. GRANT permissions to files                                                          |"
    echo "|   ${yellow}7${esc}. SHOW information of files                                                           |"
    echo "|   ${yellow}8${esc}. MOVE files between the directories                                                  |"
    echo "|   ${yellow}9${esc}. CLEAR console screen                                                                |"
    echo "|   ${yellow}10${esc}. EXIST the program                                                                  |"
    echo "${yellow}--------------------------------------------------------------------------------------------${esc}"
    echo -n "${green}CURRENT PATH: ${esc}"
    pwd
    echo "${magenta}Please SELECT option for Session!${esc}"
    echo -n "${cyan}${cyan}Enter your option: ${esc}"
    read n

    case "$n" in
        0)change_path
        ;;

        1)list_file
        ;;

        2)create_file
        ;;              

        3)write_file
        ;;  
        
        4)read_file
        ;;  
       
        5)delete_file
        ;;  

        6)grant_permission
        ;;  
        
        7)show_information_file
        ;;  

        8)move_files
        ;;  

        9) clear
        ;;
        
        10)exit_program
        ;; 
        
        
    esac
done
